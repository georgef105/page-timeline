const http = require('http');
const fs = require('fs');
const path = require('path');

const REPORTS_FOLDER = 'reports';

http.createServer((req, res) => {
  const url = req.url.replace('/api/reports', '/');

  if (url === '/') {
    fs.readdir(path.join(__dirname, REPORTS_FOLDER), (err, files) => {
      if (err) {
        res.statusCode = 500;
        res.write('Error: cant find file');
        res.end();
        return;
      }
      res.statusCode = 200;
      res.setHeader('content-type', 'application/json');
      res.write(JSON.stringify(files));
      res.end();
    });
    return;
  }

  fs.readFile(path.join(__dirname, REPORTS_FOLDER, url), (err, report) => {
    if(err) {
      res.statusCode = 500;
      res.write('Error: cant find file');
      res.end();
      return;
    }

    res.statusCode = 200;
    res.setHeader('content-type', 'application/json');
    res.write(report.toString());
    res.end();
  });
}).listen(5000);

console.log('listening to port 5000')