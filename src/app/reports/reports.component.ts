import { Component, OnInit } from '@angular/core';
import { ReportsService } from './reports.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent {
  public activeLink: string;
}
