import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';


const routes: Routes = [
  {
    path: '',
    component: ReportsComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'local'
      },
      {
        path: 'local',
        loadChildren: () => import('./local-reports/local-reports.module').then(m => m.LocalReportsModule)
      },
      {
        path: 'calibre',
        loadChildren: () => import('./calibre-reports/calibre-reports.module').then(m => m.CalibreReportsModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
