import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface LighthouseAudit {
  id: string;
  title: string;
  description: string;
  rawValue: number;
  displayValue: string;
}

export interface LighthouseReport {
  requestedUrl: string;
  audits: Record<string, LighthouseAudit>;
}

export interface CalibreReport {
  id: number;
  status: string;
  site_id: string;
  generated_at: string;
}

export interface CalibreSnapshotPageMetric {
  name: string;
  value: number;
}

export interface CalibreSnapshotPage {
  id: string;
  name: string;
  status: string;
  endpoint: string;
  metrics: Array<CalibreSnapshotPageMetric>;
}

export interface CalibreSnapshot {
  id: number;
  status: string;
  site_id: string;
  generated_at: string;
  profile: string;
  pages: Array<CalibreSnapshotPage>;
}

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getLighthouseReport(report: string): Observable<LighthouseReport> {
    return this.httpClient.get<LighthouseReport>(`/api/reports/${report}`);
  }

  public getLighthouseReports(): Observable<Array<string>> {
    return this.httpClient.get<Array<string>>('/api/reports');
  }

  public getCalibreReports(slug: string, secret: string): Observable<Array<CalibreReport>> {
    return this.httpClient.get<Array<CalibreReport>>(`/api/sites/${slug}/snapshots`, {
      params: {
        secret
      }
    });
  }

  public getCalibreReport(slug: string, snapshot: number, secret: string): Observable<CalibreSnapshot> {
    return this.httpClient.get<CalibreSnapshot>(`/api/sites/${slug}/snapshots/${snapshot}`, {
      params: {
        secret
      }
    });
  }
}
