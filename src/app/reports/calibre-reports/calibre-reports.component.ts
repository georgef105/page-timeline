import { Component, OnInit } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { ReportsService, CalibreReport } from '../reports.service';
import { ActivatedRoute } from '@angular/router';
import { map, filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-calibre-reports',
  templateUrl: './calibre-reports.component.html',
  styleUrls: ['./calibre-reports.component.scss']
})
export class CalibreReportsComponent implements OnInit {
  public calibreInfo$: Observable<{ slug: string, secret: string }>;
  public reports$: Observable<Array<CalibreReport>>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private reportsService: ReportsService
  ) { }

  ngOnInit() {
    this.calibreInfo$ = combineLatest(
      this.activatedRoute.params,
      this.activatedRoute.queryParams).pipe(
      map(([params, queryParams]) => ({ slug: params.siteSlug || '', secret: queryParams.secret || '' }))
    );

    this.reports$ = this.calibreInfo$.pipe(
      filter(calibreInfo => !!(calibreInfo.slug && calibreInfo.secret)),
      switchMap(calibreInfo => this.reportsService.getCalibreReports(calibreInfo.slug, calibreInfo.secret))
    );
  }

}
