import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalibreReportsRoutingModule } from './calibre-reports-routing.module';
import { CalibreReportsComponent } from './calibre-reports.component';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [CalibreReportsComponent],
  imports: [
    CommonModule,
    CalibreReportsRoutingModule,
    MatListModule,
    MatButtonModule,
    MatInputModule
  ]
})
export class CalibreReportsModule { }
