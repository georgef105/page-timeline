import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalibreReportsComponent } from './calibre-reports.component';


const routes: Routes = [
  {
    path: '',
    component: CalibreReportsComponent
  },
  {
    path: ':siteSlug',
    component: CalibreReportsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalibreReportsRoutingModule { }
