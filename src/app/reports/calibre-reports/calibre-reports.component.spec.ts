import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalibreReportsComponent } from './calibre-reports.component';

describe('CalibreReportsComponent', () => {
  let component: CalibreReportsComponent;
  let fixture: ComponentFixture<CalibreReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalibreReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalibreReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
