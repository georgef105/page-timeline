import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalReportsComponent } from './local-reports.component';

describe('LocalReportsComponent', () => {
  let component: LocalReportsComponent;
  let fixture: ComponentFixture<LocalReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
