import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../reports.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-local-reports',
  templateUrl: './local-reports.component.html',
  styleUrls: ['./local-reports.component.scss']
})
export class LocalReportsComponent implements OnInit {
  public reports$: Observable<Array<string>>;

  constructor(
    private reportsService: ReportsService
  ) { }

  ngOnInit() {
    this.reports$ = this.reportsService.getLighthouseReports();
  }

}
