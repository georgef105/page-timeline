import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocalReportsRoutingModule } from './local-reports-routing.module';
import { LocalReportsComponent } from './local-reports.component';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [LocalReportsComponent],
  imports: [
    CommonModule,
    LocalReportsRoutingModule,
    MatListModule,
    MatButtonModule
  ]
})
export class LocalReportsModule { }
