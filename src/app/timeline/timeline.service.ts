import { Injectable } from '@angular/core';
import { ReportsService, LighthouseReport, CalibreSnapshotPage } from '../reports/reports.service';
import { Observable } from 'rxjs';
import { TimelineEvent, TimelineMilestone } from './timeline-graph/timeline-event.interface';

import { map } from 'rxjs/operators';

const REPORT_AUDIT_ID_TO_LABEL: Record<string, string> = {
  'first-contentful-paint': 'FCP',
  'first-meaningful-paint': 'FMP',
  'time-to-first-byte': 'TTFB',
  'first-cpu-idle': 'FCI',
  'interactive': 'TTI',
  'bootup-time': 'BT'
};

const CALIBRE_METRIC_ID_TO_LABEL_MAP: Record<string, string> = {
  'visually_complete': 'VC',
  'consistently-interactive': 'CI',
  'first-interactive': 'FI',
  'time-to-first-byte': 'TTFB',
  'first-contentful-paint': 'FCP',
  'first-meaningful-paint': 'FMP',
  'firstRender': 'FR',
  'onload': 'OL',
  'oncontentload': 'OCL'
}

export interface LighthouseTimelineReport {
  timeline: Array<TimelineEvent>;
  url: string;
}

@Injectable({
  providedIn: 'root'
})
export class TimelineService {

  constructor(
    private reportsService: ReportsService
  ) { }

  public getLighthouseTimeline(reportName: string): Observable<LighthouseTimelineReport> {
    return this.reportsService.getLighthouseReport(reportName).pipe(
      map(report => {
        return {
          timeline: this.convertLighthouseReportToTimeline(report),
          url: report.requestedUrl
        };
      })
    );
  }

  public getCalibreLighthouseTimeline(slug: string, snapshotId: number, secret: string): Observable<LighthouseTimelineReport> {
    return this.reportsService.getCalibreReport(slug, snapshotId, secret).pipe(
      map(snapshot => {
        const report = snapshot.pages[0];
        return {
          timeline: this.convertCalibreLighthouseReportToTimeline(report),
          url: report.endpoint
        };
      })
    );
  }

  private convertCalibreLighthouseReportToTimeline(snapshot: CalibreSnapshotPage): Array<TimelineEvent> {
    return Object.keys(CALIBRE_METRIC_ID_TO_LABEL_MAP)
      .map(auditName => {
        const audit = snapshot.metrics.find(metric => metric.name === auditName);
        return audit && {
          id: audit.name,
          type: 'milestone',
          time: audit.value,
          label: CALIBRE_METRIC_ID_TO_LABEL_MAP[auditName],
          title: audit.name
        } as TimelineMilestone;
      })
      .filter(timeline => timeline && timeline.time)
      .sort((a, b) => {
        if (a.time < b.time) {
          return -1;
        }
        if (a.time > b.time) {
          return 1;
        }
        return 0;
      });
  }

  private convertLighthouseReportToTimeline(report: LighthouseReport): Array<TimelineEvent> {
    return Object.keys(REPORT_AUDIT_ID_TO_LABEL)
      .map(auditName => {
        const audit = report.audits[auditName];
        return audit && {
          id: audit.id,
          type: 'milestone',
          time: audit.rawValue,
          label: REPORT_AUDIT_ID_TO_LABEL[auditName],
          title: audit.title,
          description: audit.description,
          displayValue: audit.displayValue
        } as TimelineMilestone;
      })
      .filter(timeline => timeline && timeline.time)
      .sort((a, b) => {
        if (a.time < b.time) {
          return -1;
        }
        if (a.time > b.time) {
          return 1;
        }
        return 0;
      });
  }
}
