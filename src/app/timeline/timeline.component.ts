import { Component, OnInit } from '@angular/core';
import { TimelineEvent } from './timeline-graph/timeline-event.interface';
import { Observable, of, combineLatest } from 'rxjs';
import { TimelineService, LighthouseTimelineReport } from './timeline.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap, map } from 'rxjs/operators';

const REPORT_PARAM = 'report';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  public timeline$: Observable<LighthouseTimelineReport>;

  public focusedEvent: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private timelineService: TimelineService
  ) { }

  ngOnInit() {
    this.timeline$ = this.activatedRoute.data.pipe(
      switchMap(data => {
        return data.reportSource === 'local' ? this.getLocalReport() : this.getCalibreReport();
      })
    )
  }

  public onEventFocus(id: string) {
    this.focusedEvent = id;
  }

  private getLocalReport(): Observable<LighthouseTimelineReport> {
    return this.activatedRoute.params.pipe(
      switchMap(params => {
        return this.timelineService.getLighthouseTimeline(params[REPORT_PARAM]);
      })
    );
  }

  private getCalibreReport(): Observable<LighthouseTimelineReport> {
    return combineLatest(
      this.activatedRoute.params,
      this.activatedRoute.queryParams).pipe(
      map(([params, queryParams]) => ({
        slug: params.siteSlug || '',
        snapshot: params.snapshotId || 0,
        secret: queryParams.secret || ''
      })),
      switchMap(calibreInfo =>
        this.timelineService.getCalibreLighthouseTimeline(calibreInfo.slug, calibreInfo.snapshot, calibreInfo.secret))
    );
  }
}
