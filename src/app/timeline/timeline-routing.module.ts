import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimelineComponent } from './timeline.component';


const routes: Routes = [
  {
    path: '',
    component: TimelineComponent
  },
  {
    path: 'calibre/:siteSlug/:snapshotId',
    component: TimelineComponent,
    data: {
      reportSource: 'calibre'
    }
  },
  {
    path: ':report',
    component: TimelineComponent,
    data: {
      reportSource: 'local'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimelineRoutingModule { }
