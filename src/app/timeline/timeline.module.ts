import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimelineRoutingModule } from './timeline-routing.module';
import { TimelineComponent } from './timeline.component';
import { TimelineGraphModule } from './timeline-graph/timeline-graph.module';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [TimelineComponent],
  imports: [
    CommonModule,
    TimelineRoutingModule,
    TimelineGraphModule,
    MatListModule
  ]
})
export class TimelineModule { }
