import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineProcessComponent } from './timeline-process.component';



@NgModule({
  declarations: [TimelineProcessComponent],
  imports: [
    CommonModule
  ],
  exports: [
    TimelineProcessComponent
  ]
})
export class TimelineProcessModule { }
