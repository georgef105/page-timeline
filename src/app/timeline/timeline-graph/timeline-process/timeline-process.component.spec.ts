import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineProcessComponent } from './timeline-process.component';

describe('TimelineProcessComponent', () => {
  let component: TimelineProcessComponent;
  let fixture: ComponentFixture<TimelineProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
