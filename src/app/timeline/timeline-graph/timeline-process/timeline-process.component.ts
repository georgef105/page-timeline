import { Component, OnInit, Input } from '@angular/core';
import { TimelineProcess } from '../timeline-event.interface';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[timelineProcess]',
  templateUrl: './timeline-process.component.html',
  styleUrls: ['./timeline-process.component.scss']
})
export class TimelineProcessComponent {
  @Input() public timelineProcess: TimelineProcess;
  @Input() netEndTime: number;
}
