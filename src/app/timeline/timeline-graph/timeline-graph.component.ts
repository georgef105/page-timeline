import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { TimelineEvent, TimelineMilestone, TimelineProcess } from './timeline-event.interface';

@Component({
  selector: 'app-timeline-graph',
  templateUrl: './timeline-graph.component.html',
  styleUrls: ['./timeline-graph.component.scss']
})
export class TimelineGraphComponent implements OnChanges {
  @Input() public timeline: Array<TimelineEvent>;
  @Output() public eventFocus = new EventEmitter<string>();

  public endTime: number;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.timeline) {
      console.log('timeline', this.timeline);
      this.endTime = this.getTimelineEndTime(this.timeline);
    }
  }

  public getTimelineEndTime(timeline: Array<TimelineEvent>): number {
    return timeline.reduce((endTime, event) => {
      const evenFinishTime = (event as TimelineMilestone).time || (event as TimelineProcess).endTime;
      return Math.max(endTime, evenFinishTime);
    }, 0);
  }

}
