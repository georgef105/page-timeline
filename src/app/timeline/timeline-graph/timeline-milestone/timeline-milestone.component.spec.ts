import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineMilestoneComponent } from './timeline-milestone.component';

describe('TimelineMilestoneComponent', () => {
  let component: TimelineMilestoneComponent;
  let fixture: ComponentFixture<TimelineMilestoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineMilestoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineMilestoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
