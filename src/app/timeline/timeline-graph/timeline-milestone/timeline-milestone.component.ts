import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { TimelineMilestone } from '../timeline-event.interface';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[timelineMilestone]',
  templateUrl: './timeline-milestone.component.html',
  styleUrls: ['./timeline-milestone.component.scss']
})
export class TimelineMilestoneComponent implements OnChanges {
  @Input() public timelineMilestone: TimelineMilestone;
  @Input() netEndTime: number;
  @Output() eventFocus = new EventEmitter<string>();

  public xPosition: number;

  public ngOnChanges(): void {
    this.xPosition = (this.timelineMilestone.time / this.netEndTime) * 100;
  }

  public onMouseOver(): void {
    this.eventFocus.emit(this.timelineMilestone.id);
  }
}
