import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineMilestoneComponent } from './timeline-milestone.component';



@NgModule({
  declarations: [TimelineMilestoneComponent],
  imports: [
    CommonModule
  ],
  exports: [
    TimelineMilestoneComponent
  ]
})
export class TimelineMilestoneModule { }
