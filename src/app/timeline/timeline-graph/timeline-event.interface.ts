export interface TimelineEventBase {
  id: string;
  type: string;
  label: string;
  title: string;
  description?: string;
  displayValue?: string;
}

export interface TimelineMilestone extends TimelineEventBase {
  type: 'milestone';
  time: number;
}

export interface TimelineProcess extends TimelineEventBase {
  type: 'process';
  startTime: number;
  endTime: number;
}

export type TimelineEvent = TimelineMilestone | TimelineProcess;

