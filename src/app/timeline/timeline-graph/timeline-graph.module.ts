import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineGraphComponent } from './timeline-graph.component';
import { TimelineMilestoneModule } from './timeline-milestone/timeline-milestone.module';
import { TimelineProcessModule } from './timeline-process/timeline-process.module';

@NgModule({
  declarations: [TimelineGraphComponent],
  imports: [
    CommonModule,
    TimelineMilestoneModule,
    TimelineProcessModule
  ],
  exports: [TimelineGraphComponent]
})
export class TimelineGraphModule { }
